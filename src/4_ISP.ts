/**
 * Principio de segregación de interfaz: establece que los clientes no deberían verse forzados a depender de interfaces que no usan
 * ya que esto hace que sean afectados por los cambios que otros clientes necesiten hacer en esta interfaz.
 */

/**
 * Se crean diferentes interfaces para inplementar métodos que solo lo necesiten sin implementar interfaces innecesarias
 * @interface IBird : se crea la interfaz que comparten todas las clases
 * @interface IFlyingBird : se crea la interfaz que comparten solo las aves voladoras
 * @interface ISwimmingBird : se crea la interfaz que comparten solo las aves nadadoras
 */

interface IBird {
    eat():void;
}

interface IFlyingBird {
    fly():void;
}

interface ISwimmingBird {
   swim():void;
}

class Eagle implements IBird,IFlyingBird{
    //create object to add properties
    private atributes:{
        speed: number;
        food: string[];
        swim:boolean
    } = {
        speed:0,
        food: [],
        swim: false
    };

    eat(){
        this.atributes.food = ["mice","fish","snakes","birds"];
    }

    fly() {
        this.atributes.speed = 320;
    }

}

class Pelican implements IBird,ISwimmingBird,IFlyingBird{
    private atributes:{
        speed: number;
        food: string[];
        swim: boolean
    } = {
        speed:0,
        food: [],
        swim:false
    };

    eat() {
        this.atributes.food = ["fish"];
    }

    fly() {
        this.atributes.speed = 60;
    }

    swim() {
        this.atributes.swim = true
    }
}
//funcion inicializadora
(()=>{
    const eagle = new Eagle();
    eagle.eat();
    eagle.fly();
    const pelican = new Pelican();
    pelican.eat();
    pelican.swim();
    pelican.fly()
    console.log(eagle);
    console.log(pelican);
})()


