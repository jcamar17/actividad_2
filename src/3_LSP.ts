/**
 * Principio de substitución de Liskov: una subclase debe ser sustituible por su superclase, y si al hacer esto, el programa falla, estaremos violando este principio.
 * Cumpliendo con este principio se confirmará que nuestro programa tiene una jerarquía de clases fácil de entender y un código reutilizable.
*/

/**
 * @class DefaultCar : se crea esta clase padre la cual se maneja como clase abstracta para evitar su modificación y que las clases hijas hereden los comportamientos necesarios de esta clase
 * cumpliendo con el principio
 */

abstract class DefaultCar{
    private readonly _model:string;
    private readonly _price:number;

    constructor(model:string,price:number){
        this._model = model;
        this._price = price;
    }

    get model(): string {
        return this._model;
    }

    get price(): number {
        return this._price;
    }

}

class Audi extends DefaultCar{
    private readonly _seats;

    constructor(model:string,price:number,seats:number){
        super(model, price);
        this._seats = seats;
    }

    get seats():string{
        return this._seats;
    }
}

class McLaren extends DefaultCar{
    private readonly _seats;

    constructor(model:string,price:number,seats:number){
        super(model, price);
        this._seats = seats;
    }

    get seats():string{
        return this._seats;
    }
}

(()=>{
    const cars = [
        new Audi("A4",50000,4),
        new McLaren("P1",1200000,2)
    ];

    cars.forEach( car => console.log({ car: car.model, seats: car.seats }) );
})();









