/***
 * Principio de responsabilidad única - SRP : una clase, componente o servicio debe ser responsable de una sola cosa para
 * evitar que las modificaciones en varias responsabilidades generen errores o bugs en el programa.
 * */

//Se realiza la importación del modelo
import {car} from "./model/car";

//variable que se utilizará como "base de datos"
const cars: car[] = [];

//Se crea la clase car
class Car{
    private _model:string;
    private _year:number;

    constructor(model:string,year:number) {
        this._model = model;
        this._year = year;
    }

    get model(): string {
        return this._model;
    }

    set model(value: string) {
        this._model = value;
    }

    get year(): number {
        return this._year;
    }

    set year(value: number) {
        this._year = value;
    }
}

//Se crea la clase que contiene la responsabilidad de almacenar los vehiculos en la base de datos, cumpliendo de esta manera con el principio
export class CarDB {

    saveCar(car:Car):void{
        cars.push(car);
        console.log(`car ${car.model} saved`);
    }

    getCars():car[]{
        return cars;
    }
}

// se instancia las clases para probar su usabilidad
const car1 = new Car("BMW X6",2020);
const db = new CarDB();
db.saveCar(car1);

const car2 = new Car("Ford Raptor",2020);
db.saveCar(car2);
const carList = db.getCars();
console.log(carList);
