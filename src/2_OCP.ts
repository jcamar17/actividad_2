/**
 * Principio abierto/cerrado - OCP : indica que las entidades de software (modelos,clases y funciones)
 * deberían estar abiertas para su extensión, pero cerrados para su modificación
 */

/*Se crea la clase padre Car la cual al ser abstracta, permite que los métodos y atributos puedan ser
instanciados y sobreescritos por las clases que los llaman, pero no permite la edición del mismo directamente, cumpliendo con el principio*/

abstract class Car{
    protected _model:string;
    protected _price:number;

    protected constructor(model:string, price:number){
        this._model = model;
        this._price = price;
    }

    priceCar():void{};

    get model(): string {
        return this._model;
    }
}
/*Solo las clases que heredan, pueden acceder a sus métodos y propiedades y sobreescribirlos según su necesidad en este caso, el impuesto se maneja como una constante sobre la clase
* y cada marca lo maneja de manera independiente según su politica interna*/

class Renault extends Car{
    private tax:number = 50;
    constructor(model:string,price:number){
        super(model,price);
    }

    priceCar(): number {
        return this._price + (this.tax/50);
    }
}

class Bmw extends Car{
    private tax:number = 100;
    constructor(model:string,price:number){
        super(model,price);
    }

    priceCar(): number {
        return this._price +this.tax;
    }
}

class Mercedes extends Car{
    private tax:number = 200;
    constructor(model:string,price:number){
        super(model,price);
    }

    priceCar(): number {
        return this._price + (this.tax*0.05);
    }
}

// se instancia las clases para probar su usabilidad
const cars: Car[] = [
    new Bmw("M3",25000),
    new Mercedes("SLK200",35000),
    new Renault("CLIO",6000)
];

//funcion para imprimir el arreglo de carros
function printPriceCar():void {
    for(let car of cars){
        console.log(`${car.model} :$${car.priceCar()}`);
    }
}

printPriceCar();
