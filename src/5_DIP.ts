/**
 * Principio de inversión de dependencias - DIP: Establece que las dependencias deben estar en las abstracciones, no en las concreciones. Es decir:
    Los módulos de alto nivel no deberían depender de módulos de bajo nivel. Ambos deberían depender de abstracciones.
    Las abstracciones no deberían depender de detalles. Los detalles deberían depender de abstracciones.
 */
import {ajax} from "rxjs/ajax";
import {Observable} from "rxjs";
import { XMLHttpRequest } from "xmlhttprequest";
import {pluck} from "rxjs/operators";
import { users } from "./model/users.mock";

/***
 * @interface IConnect: se crea la interfaz que contiene los métodos que se pueden abstraer de las dos clases ya que su implementación es totalmente distinta, cumpliendo con el principio DIP
 */

interface IConnect {
   getData():any;
   setData(data:object):void;
}
//Se realiza una implementación que consume un servicio de una API (https://rickandmortyapi.com/)
class ApiService implements IConnect{
    private readonly _url:string;
    private _data:object = {};

    constructor(url:string){
        this._url = url;
    }

    private createXHR() {
        return new XMLHttpRequest();
    }

    queryApi():Observable<object>{
        return ajax({
            url: this._url,
            crossDomain:true,
            createXHR: this.createXHR
        }).pipe(
            pluck('response')
        );
    }

    getData(): object {
        return this._data;
    }

    setData(data:object):void {
        this._data = data;
    }
}

//Se realiza un servicio que consume un servicio de "bases de datos (user.mock)"
class DatabaseService implements IConnect{
    private users = users;
    getData(){
        return this.users;
    }

    setData(data: object) {
        this.users.push(data);
    }
}

//función inicializadora
(()=>{
    //se crea un observable : https://rxjs-dev.firebaseapp.com/guide/overview, primero se ejecutará el codigo síncrono(const database) y luego el código asíncrono (apiService)
    const api = new ApiService("https://rickandmortyapi.com/api/character/183");
    const $obs = api.queryApi();
    $obs.subscribe( data => {
        api.setData(data);
        console.log(api.getData());
    });
    const database = new DatabaseService();
    database.setData({name:"José rojas",age:23,ocupation:"Quality Assurance"},)
    console.log(database.getData());
})()
