"use strict";
/***
 * Principio de responsabilidad única - SRP : una clase, componente o servicio debe ser responsable de una sola cosa para
 * evitar que las modificaciones en varias responsabilidades generen errores o bugs en el programa.
 * */
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarDB = void 0;
//variable que se utilizará como "base de datos"
var cars = [];
//Se crea la clase car
var Car = /** @class */ (function () {
    function Car(model, year) {
        this._model = model;
        this._year = year;
    }
    Object.defineProperty(Car.prototype, "model", {
        get: function () {
            return this._model;
        },
        set: function (value) {
            this._model = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Car.prototype, "year", {
        get: function () {
            return this._year;
        },
        set: function (value) {
            this._year = value;
        },
        enumerable: false,
        configurable: true
    });
    return Car;
}());
//Se crea la clase que contiene la responsabilidad de almacenar los vehiculos en la base de datos, cumpliendo de esta manera con el principio
var CarDB = /** @class */ (function () {
    function CarDB() {
    }
    CarDB.prototype.saveCar = function (car) {
        cars.push(car);
        console.log("car " + car.model + " saved");
    };
    CarDB.prototype.getCars = function () {
        return cars;
    };
    return CarDB;
}());
exports.CarDB = CarDB;
// se instancia las clases para probar su usabilidad
var car1 = new Car("BMW X6", 2020);
var db = new CarDB();
db.saveCar(car1);
var car2 = new Car("Ford Raptor", 2020);
db.saveCar(car2);
var carList = db.getCars();
console.log(carList);
//# sourceMappingURL=1_SRP.js.map