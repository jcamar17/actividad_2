"use strict";
/**
 * Principio abierto/cerrado - OCP : indica que las entidades de software (modelos,clases y funciones)
 * deberían estar abiertas para su extensión, pero cerrados para su modificación
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*Se crea la clase padre Car la cual al ser abstracta, permite que los métodos y atributos puedan ser
instanciados y sobreescritos por las clases que los llaman, pero no permite la edición del mismo directamente, cumpliendo con el principio*/
var Car = /** @class */ (function () {
    function Car(model, price) {
        this._model = model;
        this._price = price;
    }
    Car.prototype.priceCar = function () { };
    ;
    Object.defineProperty(Car.prototype, "model", {
        get: function () {
            return this._model;
        },
        enumerable: false,
        configurable: true
    });
    return Car;
}());
/*Solo las clases que heredan, pueden acceder a sus métodos y propiedades y sobreescribirlos según su necesidad en este caso, el impuesto se maneja como una constante sobre la clase
* y cada marca lo maneja de manera independiente según su politica interna*/
var Renault = /** @class */ (function (_super) {
    __extends(Renault, _super);
    function Renault(model, price) {
        var _this = _super.call(this, model, price) || this;
        _this.tax = 50;
        return _this;
    }
    Renault.prototype.priceCar = function () {
        return this._price + (this.tax / 50);
    };
    return Renault;
}(Car));
var Bmw = /** @class */ (function (_super) {
    __extends(Bmw, _super);
    function Bmw(model, price) {
        var _this = _super.call(this, model, price) || this;
        _this.tax = 100;
        return _this;
    }
    Bmw.prototype.priceCar = function () {
        return this._price + this.tax;
    };
    return Bmw;
}(Car));
var Mercedes = /** @class */ (function (_super) {
    __extends(Mercedes, _super);
    function Mercedes(model, price) {
        var _this = _super.call(this, model, price) || this;
        _this.tax = 200;
        return _this;
    }
    Mercedes.prototype.priceCar = function () {
        return this._price + (this.tax * 0.05);
    };
    return Mercedes;
}(Car));
// se instancia las clases para probar su usabilidad
var cars = [
    new Bmw("M3", 25000),
    new Mercedes("SLK200", 35000),
    new Renault("CLIO", 6000)
];
//funcion para imprimir el arreglo de carros
function printPriceCar() {
    for (var _i = 0, cars_1 = cars; _i < cars_1.length; _i++) {
        var car = cars_1[_i];
        console.log(car.model + " :$" + car.priceCar());
    }
}
printPriceCar();
//# sourceMappingURL=2_OCP.js.map