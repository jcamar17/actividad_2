"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Principio de inversión de dependencias - DIP: Establece que las dependencias deben estar en las abstracciones, no en las concreciones. Es decir:
    Los módulos de alto nivel no deberían depender de módulos de bajo nivel. Ambos deberían depender de abstracciones.
    Las abstracciones no deberían depender de detalles. Los detalles deberían depender de abstracciones.
 */
var ajax_1 = require("rxjs/ajax");
var xmlhttprequest_1 = require("xmlhttprequest");
var operators_1 = require("rxjs/operators");
var users_mock_1 = require("./model/users.mock");
//Se realiza una implementación que consume un servicio de una API (https://rickandmortyapi.com/)
var ApiService = /** @class */ (function () {
    function ApiService(url) {
        this._data = {};
        this._url = url;
    }
    ApiService.prototype.createXHR = function () {
        return new xmlhttprequest_1.XMLHttpRequest();
    };
    ApiService.prototype.queryApi = function () {
        return ajax_1.ajax({
            url: this._url,
            crossDomain: true,
            createXHR: this.createXHR
        }).pipe(operators_1.pluck('response'));
    };
    ApiService.prototype.getData = function () {
        return this._data;
    };
    ApiService.prototype.setData = function (data) {
        this._data = data;
    };
    return ApiService;
}());
//Se realiza un servicio que consume un servicio de "bases de datos (user.mock)"
var DatabaseService = /** @class */ (function () {
    function DatabaseService() {
        this.users = users_mock_1.users;
    }
    DatabaseService.prototype.getData = function () {
        return this.users;
    };
    DatabaseService.prototype.setData = function (data) {
        this.users.push(data);
    };
    return DatabaseService;
}());
//función inicializadora
(function () {
    //se crea un observable : https://rxjs-dev.firebaseapp.com/guide/overview, primero se ejecutará el codigo síncrono(const database) y luego el código asíncrono (apiService)
    var api = new ApiService("https://rickandmortyapi.com/api/character/183");
    var $obs = api.queryApi();
    $obs.subscribe(function (data) {
        api.setData(data);
        console.log(api.getData());
    });
    var database = new DatabaseService();
    database.setData({ name: "José rojas", age: 23, ocupation: "Quality Assurance" });
    console.log(database.getData());
})();
//# sourceMappingURL=5_DIP.js.map