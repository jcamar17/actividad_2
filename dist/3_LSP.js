"use strict";
/**
 * Principio de substitución de Liskov: una subclase debe ser sustituible por su superclase, y si al hacer esto, el programa falla, estaremos violando este principio.
 * Cumpliendo con este principio se confirmará que nuestro programa tiene una jerarquía de clases fácil de entender y un código reutilizable.
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * @class DefaultCar : se crea esta clase padre la cual se maneja como clase abstracta para evitar su modificación y que las clases hijas hereden los comportamientos necesarios de esta clase
 * cumpliendo con el principio
 */
var DefaultCar = /** @class */ (function () {
    function DefaultCar(model, price) {
        this._model = model;
        this._price = price;
    }
    Object.defineProperty(DefaultCar.prototype, "model", {
        get: function () {
            return this._model;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DefaultCar.prototype, "price", {
        get: function () {
            return this._price;
        },
        enumerable: false,
        configurable: true
    });
    return DefaultCar;
}());
var Audi = /** @class */ (function (_super) {
    __extends(Audi, _super);
    function Audi(model, price, seats) {
        var _this = _super.call(this, model, price) || this;
        _this._seats = seats;
        return _this;
    }
    Object.defineProperty(Audi.prototype, "seats", {
        get: function () {
            return this._seats;
        },
        enumerable: false,
        configurable: true
    });
    return Audi;
}(DefaultCar));
var McLaren = /** @class */ (function (_super) {
    __extends(McLaren, _super);
    function McLaren(model, price, seats) {
        var _this = _super.call(this, model, price) || this;
        _this._seats = seats;
        return _this;
    }
    Object.defineProperty(McLaren.prototype, "seats", {
        get: function () {
            return this._seats;
        },
        enumerable: false,
        configurable: true
    });
    return McLaren;
}(DefaultCar));
(function () {
    var cars = [
        new Audi("A4", 50000, 4),
        new McLaren("P1", 1200000, 2)
    ];
    cars.forEach(function (car) { return console.log({ car: car.model, seats: car.seats }); });
})();
//# sourceMappingURL=3_LSP.js.map