"use strict";
/**
 * Principio de segregación de interfaz: establece que los clientes no deberían verse forzados a depender de interfaces que no usan
 * ya que esto hace que sean afectados por los cambios que otros clientes necesiten hacer en esta interfaz.
 */
var Eagle = /** @class */ (function () {
    function Eagle() {
        //create object to add properties
        this.atributes = {
            speed: 0,
            food: [],
            swim: false
        };
    }
    Eagle.prototype.eat = function () {
        this.atributes.food = ["mice", "fish", "snakes", "birds"];
    };
    Eagle.prototype.fly = function () {
        this.atributes.speed = 320;
    };
    return Eagle;
}());
var Pelican = /** @class */ (function () {
    function Pelican() {
        this.atributes = {
            speed: 0,
            food: [],
            swim: false
        };
    }
    Pelican.prototype.eat = function () {
        this.atributes.food = ["fish"];
    };
    Pelican.prototype.fly = function () {
        this.atributes.speed = 60;
    };
    Pelican.prototype.swim = function () {
        this.atributes.swim = true;
    };
    return Pelican;
}());
//funcion inicializadora
(function () {
    var eagle = new Eagle();
    eagle.eat();
    eagle.fly();
    var pelican = new Pelican();
    pelican.eat();
    pelican.swim();
    pelican.fly();
    console.log(eagle);
    console.log(pelican);
})();
//# sourceMappingURL=4_ISP.js.map