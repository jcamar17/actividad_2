# Actividad 2 - Implementación de principios SOLID y GRASP
## _por Juan Pablo Camargo Vanegas_

### Ejercicios desarrollados en Typescript

#### _**para ejecutar el proyecto:**_
    * npm install
    * node dist/'nombre del archivo'.js (ejemplo: node dist/1_SRP.js)

bibliografía:
_https://enmilocalfunciona.io/principios-solid/_


##Captura de ejercicios en funcionamiento

**Ejercicio 1 - SRP**

[1 SRP](https://laiberocol-my.sharepoint.com/:i:/g/personal/jcamar17_ibero_edu_co/EQOJlFQ3o11OjDUPF1MylpMBsttHVWGrXNkff1YCyoBNGA?e=cZ2HEf)


**Ejercicio 2 - OCP**

[2 OCP](https://laiberocol-my.sharepoint.com/:i:/g/personal/jcamar17_ibero_edu_co/EYPNhrmb3-FFu3rnUjQmm-cBXyINaJOlf31fMPRgY46gxA?e=aO0maE)


**Ejercicio 3 - LSP**

[3 LSP](https://laiberocol-my.sharepoint.com/:i:/g/personal/jcamar17_ibero_edu_co/Ee_C6AiVQAdNiaZywireW6MBqAnNT5HK_AoBtzZHuJ4KMQ?e=5XcyfD)


**Ejercicio 4 - ISP**

[4 ISP](https://laiberocol-my.sharepoint.com/:i:/g/personal/jcamar17_ibero_edu_co/ES5ifloSxqNPu3bHAsKY0hABndX9SB3_BySgWog9eqxzxg?e=bdFIt9)


**Ejercicio 5 - DIP**

[5 DIP](https://laiberocol-my.sharepoint.com/:i:/g/personal/jcamar17_ibero_edu_co/EenJmOVHmf1MpJLuyeYzDC8B21SnmF5M1R-5tpgZcGMApA?e=Jsgcbt)
